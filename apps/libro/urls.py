from django.urls import path
from .views import crear_autor, listar_autores

urlpatterns = [
    path('listar_autores/', listar_autores, name='listar_autores'),
    # path('autor/<int:id>', listar_autores, name='listar_autores'),
    path('crear_autor/', crear_autor, name='crear_autor')
]
